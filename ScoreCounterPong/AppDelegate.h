//
//  AppDelegate.h
//  ScoreCounterPong
//
//  Created by Randy Jorgensen on 1/20/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

